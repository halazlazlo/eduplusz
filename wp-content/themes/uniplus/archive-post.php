<?php

/*
Template Name: Post list
*/

$posts = get_posts([
    'post_type' => 'post'
]);

header("Location: /".$posts[0]->post_name);
die();
