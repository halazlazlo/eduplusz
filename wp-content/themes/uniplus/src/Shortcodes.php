<?php

namespace Uniplus {

    use HL\WPAutoloader\Shortcodes as Base;

    class Shortcodes extends Base
    {
        public function init_vc_shortcodes()
        {
            $this->vcShortcodes = [
                ///////////////
                // Overrides //
                ///////////////

                // section
                [
                    'name'                    => __('Section', get_template()),
                    'base'                    => 'vc_row',
                    'is_container'            => true,
                    'icon'                    => 'icon-wpb-row',
                    'show_settings_on_create' => false,
                    'category'                => __('Layout', get_template()),
                    'description'             => __('Wrapper for elements', get_template()),
                    'params'                  => [
                        [
                            'type'       => 'dropdown',
                            'heading'    => __( 'Background color', get_template()),
                            'param_name' => 'bg',
                            'value'      => [
                                __('None', get_template())  => '',
                                __('White', get_template()) => 'white'
                            ]
                        ],
                        /* [
                            'type'        => 'checkbox',
                            'heading'     => __('Add margin on top?', get_template()),
                            'param_name'  => 'mt',
                            'value'       => [
                                __('Yes', get_template()) => '1'
                            ],
                        ], */
                        [
                            'type'        => 'checkbox',
                            'heading'     => __('Remove padding on top?', get_template()),
                            'param_name'  => 'rm_pt',
                            'value'       => [
                                __('Yes', get_template()) => '1'
                            ],
                        ],
                        [
                            'type'        => 'checkbox',
                            'heading'     => __('Remove padding on bottom?', get_template()),
                            'param_name'  => 'rm_pb',
                            'value'       => [
                                __('Yes', get_template()) => '1'
                            ],
                        ],
                        [
                            'type'        => 'attach_image',
                            'heading'     => __('Background image', get_template()),
                            'param_name'  => 'bg_img'
                        ],
                        [
                            'type'        => 'checkbox',
                            'heading'     => __('Show dots on background image?', get_template()),
                            'param_name'  => 'bg_img_overlay',
                            'value'       => [
                                __('Yes', get_template()) => '1'
                            ],
                        ],
                        [
                            'type'        => 'checkbox',
                            'heading'     => __('Show contact info in this block?', get_template()),
                            'param_name'  => 'show_contact_link',
                            'value'       => [
                                __('Yes', get_template()) => '1'
                            ],
                        ]
                    ],
                    'js_view' => 'VcRowView',
                ],

                /////////////
                // Content //
                /////////////

                // heading
                [
                    'name'        => __('Heading', get_template()),
                    'base'        => 'heading',
                    'category'    => __('Content', get_template()),
                    'icon'        => 'icon-wpb-layer-shape-text',
                    'params'      => [
                        [
                            'param_name'  => 'txt',
                            'type'        => 'textfield',
                            'heading'     => __('Content', get_template()),
                            'holder'      => 'div',
                        ],
                        [
                            'type'        => 'dropdown',
                            'heading'     => __('Type', get_template()),
                            'param_name'  => 'class',
                            'value'       => [
                                __('Heading 2', get_template()) => '2',
                                __('Heading 3', get_template()) => '3',
                                __('Heading 4', get_template()) => '4',
                            ]
                        ],
                        [
                            'type'        => 'dropdown',
                            'heading'     => __('Align content', get_template()),
                            'param_name'  => 'align',
                            'value'       => [
                                __('None (left)', get_template()) => 'left',
                                __('Center', get_template()) => 'center',
                                __('Right', get_template()) => 'right',
                            ],
                            'holder' => 'div'
                        ]
                    ],
                ],

                // section intro
                [
                    'name'        => __('Section intro', get_template()),
                    'base'        => 'section_intro',
                    'category'    => __('Content', get_template()),
                    'icon'        => 'fa fa-align-center',
                    'params'      => [
                        [
                            'param_name'  => 'content',
                            'type'        => 'textarea_html',
                            'heading'     => __('Content', get_template()),
                            'description' => __('The content', get_template()),
                            'holder'      => 'div',
                        ]
                    ],
                ],

                // txt
                [
                    'name'        => __('Text', get_template()),
                    'base'        => 'txt',
                    'category'    => __('Content', get_template()),
                    'icon'        => 'fa fa-font',
                    'params'      => [
                        [
                            'param_name'  => 'color',
                            'type'        => 'dropdown',
                            'heading'     => __('Color', get_template()),
                            'holder'      => 'div',
                            'value'      => [
                                __('Default', get_template()) => '',
                                __('Green (Apple)', get_template())   => 'apple'
                            ]
                        ],
                        [
                            'param_name'  => 'content',
                            'type'        => 'textarea_html',
                            'heading'     => __('Content', get_template()),
                            'holder'      => 'div',
                        ]
                    ],
                ],

                // Button
                [
                    'name'        => __('Button', get_template()),
                    'base'        => 'btn',
                    'category'    => __('Content', get_template()),
                    'icon'        => 'fa fa-link',
                    'params'      => [
                        [
                            'param_name'  => 'href',
                            'type'        => 'vc_link',
                            'heading'     => __('Button url', get_template()),
                            'description' => __('Onclick label/action', get_template()),
                            'holder'      => 'div',
                        ]
                    ],
                ],

                ////////////////
                // Components //
                ////////////////

                // Slider (standard)
                [
                    'name'        => __('Slider (standard)', get_template()),
                    'base'        => 'slider_std',
                    'category'    => __('Components', get_template()),
                    'description' => __('Like homepage top', get_template()),
                    'icon'        => 'fa fa-slideshare',
                    'params'      => [
                        [
                            'param_name'  => 'imgs',
                            'type'        => 'attach_images',
                            'heading'     => __('Images url', get_template()),
                            'description' => __('Images shown', get_template()),
                            'holder'      => 'div',
                        ]
                    ],
                ],

                // Slider (Images)
                [
                    'name'                    => __('Slider (Images)', get_template()),
                    'base'                    => 'slider_img',
                    'category'                => __('Components', get_template()),
                    'description'             => __('Like discounts', get_template()),
                    'icon'                    => 'fa fa-slideshare',
                    "content_element"         => true,
                    "as_parent"               => array('only' => 'slider_img_item'),
                    "js_view"                 => 'VcColumnView',
                    'params' => [
                        [
                            'type'        => 'checkbox',
                            'heading'     => __('Add some margin on top?', get_template()),
                            'param_name'  => 'mt',
                            'value'       => [
                                __('Yes', get_template()) => '1'
                            ],
                        ]
                    ]
                ],
                [
                    'name'            => __('Slide', get_template()),
                    'base'            => 'slider_img_item',
                    'category'        => __('Content', get_template()),
                    "content_element" => true,
                    'icon'            => 'fa fa-slideshare',
                    "as_child"       => array('only' => 'timeline'),
                    'params'          => [
                        [
                            'type'        => 'attach_image',
                            'param_name'  => 'img',
                            'heading'     => __('Image', get_template()),
                            'description' => __('Image shown on the top', get_template()),
                            'holder'      => 'div',
                        ],
                        [
                            "type"        => "textfield",
                            "param_name"  => "title",
                            "heading"     => __("Title", get_template()),
                            "description" => __("The title of the item", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textarea_html",
                            "param_name"  => "content",
                            "holder"      => "div",
                            "heading"     => __("Content", get_template()),
                            "description" => __("The description of the image", get_template())
                        ]
                    ]
                ],

                // Slider (Gallery)
                [
                    'name'                    => __('Slider (Gallery)', get_template()),
                    'base'                    => 'slider_gallery',
                    'category'                => __('Components', get_template()),
                    'description'             => __('Like homepage gallery', get_template()),
                    'icon'                    => 'fa fa-slideshare',
                    "content_element"         => true,
                    "as_parent"               => array('only' => 'slider_gallery_item'),
                    "js_view"                 => 'VcColumnView',
                    "show_settings_on_create" => false,
                ],
                [
                    'name'            => __('Slide', get_template()),
                    'base'            => 'slider_gallery_item',
                    'category'        => __('Content', get_template()),
                    'description'     => __('Slider item', get_template()),
                    "content_element" => true,
                    'icon'            => 'fa fa-file-image-o',
                    "as_child"        => array('only' => 'slider_gallery'),
                    'params'          => [
                        [
                            'type'        => 'attach_image',
                            'param_name'  => 'img',
                            'heading'     => __('Image', get_template()),
                            'description' => __('Image shown on the top', get_template()),
                            'holder'      => 'div',
                        ],
                        [
                            "type"        => "textfield",
                            "param_name"  => "title",
                            "heading"     => __("Title", get_template()),
                            "description" => __("The title of the item", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "vc_link",
                            "param_name"  => "link",
                            "heading"     => __("Url", get_template()),
                            "description" => __("The link", get_template()),
                            "holder"      => "div",
                        ]
                    ]
                ],

                // Contact link
                [
                    'name'                    => __('Contact link', get_template()),
                    'base'                    => 'contact_link',
                    'category'                => __('Components', get_template()),
                    'description'             => __('A section with the email and a button to contact page', get_template()),
                    'icon'                    => 'fa fa-envelope-o',
                    'show_settings_on_create' => false
                ],

                // Contact line
                [
                    'name'                    => __('Contact line', get_template()),
                    'base'                    => 'contact_line',
                    'category'                => __('Components', get_template()),
                    'description'             => __('Contact infos in a line, like contact page top', get_template()),
                    'icon'                    => 'fa fa-envelope-o',
                    'show_settings_on_create' => false
                ],

                // Forms
                [
                    'name'                    => __('Contact form', get_template()),
                    'base'                    => 'contact_form',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-envelope-o',
                    'show_settings_on_create' => false
                ],
                [
                    'name'                    => __('Survey form', get_template()),
                    'base'                    => 'survey_form',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-envelope-o',
                    'show_settings_on_create' => false
                ],
                [
                    'name'                    => __('Malfunction form', get_template()),
                    'base'                    => 'malfunction_form',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-envelope-o',
                    'show_settings_on_create' => false
                ],

                // Map
                [
                    'name'                    => __('Map', get_template()),
                    'base'                    => 'map',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-map-o',
                    'show_settings_on_create' => false
                ],

                // Infographic (process)
                [
                    'name'                    => __('Infographics (process)', get_template()),
                    'base'                    => 'infographics_process',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-file-image-o',
                    'params' => [
                        [
                            "type"        => "textfield",
                            "param_name"  => "col_left_title",
                            "heading"     => __("Left title", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textarea",
                            "param_name"  => "col_left_desc",
                            "heading"     => __("Left description", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textfield",
                            "param_name"  => "col_mid_title",
                            "heading"     => __("Middle title", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textarea",
                            "param_name"  => "col_mid_left_desc",
                            "heading"     => __("Middle left description", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textarea",
                            "param_name"  => "col_mid_mid_desc",
                            "heading"     => __("Middle middile description", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textarea",
                            "param_name"  => "col_mid_right_desc",
                            "heading"     => __("Middle right description", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textfield",
                            "param_name"  => "col_right_title",
                            "heading"     => __("Right title", get_template()),
                            "holder"      => "div",
                        ],
                        [
                            "type"        => "textarea",
                            "param_name"  => "col_right_desc",
                            "heading"     => __("Right description", get_template()),
                            "holder"      => "div",
                        ],
                    ]
                ],

                // Gallery
                [
                    'name'                    => __('Gallery', get_template()),
                    'base'                    => 'gallery',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-picture-o',
                    'params' => [
                        [
                            "type"        => "attach_images",
                            "param_name"  => "img_ids",
                            "heading"     => __("Images", get_template()),
                        ]
                    ]
                ],

                // Gallery thumbnails
                [
                    'name'        => __('Gallery thumbnails', get_template()),
                    'base'        => 'gallery_thumbs',
                    'category'    => __('Components', get_template()),
                    'description' => __('Like posts', get_template()),
                    'icon'        => 'fa fa-picture-o',
                    'params'      => [
                        [
                            "type"        => "attach_images",
                            "param_name"  => "img_ids",
                            "heading"     => __("Images", get_template()),
                        ]
                    ]
                ],

                // Example
                [
                    'name'                    => __('Examples', get_template()),
                    'base'                    => 'examples',
                    'category'                => __('Components', get_template()),
                    'description'             => __('An example of costs', get_template()),
                    'icon'                    => 'fa fa-money',
                    'show_settings_on_create' => false,
                    "content_element"         => true,
                    "as_parent"               => array('only' => 'example'),
                    "js_view"                 => 'VcColumnView',
                    'params' => [
                        [
                            'type'        => "textarea",
                            'param_name'  => "ps",
                            'heading'     => __("Ps", get_template()),
                            'holder'      => "div",
                        ]
                    ]
                ],
                [
                    'name'            => __('Example', get_template()),
                    'base'            => 'example',
                    'category'        => __('Components', get_template()),
                    'icon'            => 'fa fa-money',
                    'content_element' => true,
                    'as_child'        => [
                        'only' => 'examples'
                    ],
                    'params'          => [
                        [
                            'type'        => "textfield",
                            'param_name'  => "category",
                            'heading'     => __("Category", get_template()),
                            'holder'      => "div",
                        ],
                        [
                            'type'        => "textfield",
                            'param_name'  => "normal",
                            'heading'     => __("Normal price", get_template()),
                            'holder'      => "div",
                        ],
                        [
                            'type'        => "textfield",
                            'param_name'  => "edu",
                            'heading'     => __("Edu+ price", get_template()),
                            'holder'      => "div",
                        ]
                    ]
                ],

                // Advantages
                [
                    'name'                    => __('Advantages', get_template()),
                    'base'                    => 'advantages',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-thumbs-o-up',
                    'show_settings_on_create' => false
                ],

                // Logos
                [
                    'name'                    => __('Logo grid', get_template()),
                    'base'                    => 'logo_grid',
                    'category'                => __('Components', get_template()),
                    'icon'                    => 'fa fa-cogs',
                    'params' => [
                        [
                            'param_name'  => 'img_ids',
                            'type'        => 'attach_images',
                            'heading'     => __('Images', get_template()),
                            'holder'      => 'div',
                        ]
                    ]
                ]
            ];
        }
    }
}

namespace {

    if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
        class WPBakeryShortCode_Slider_Img extends WPBakeryShortCodesContainer {
        }
    }

    if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
        class WPBakeryShortCode_Slider_Gallery extends WPBakeryShortCodesContainer {
        }
    }

    if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
        class WPBakeryShortCode_Examples extends WPBakeryShortCodesContainer {
        }
    }
}
