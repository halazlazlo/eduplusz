<?php

namespace Uniplus\Utils;

use HL\WPAutoloader\Utils\MenuUtil as BaseUtil;

class MenuUtil extends BaseUtil {

    public function start_el( &$output, $element, $depth = 0, $args = array(), $current_object_id = 0 )
    {
        global $post;
        $liClass = 'menu__item';
        $liClasses = [$liClass];

        $linkClass = isset($args->link_class) ? $args->link_class : null;
        $linkClasses = ['menu__link', $linkClass];

        if ($element->current || ($element->ID === 20 && $post->post_type === 'post')) {
            $linkClasses[] = 'menu__link--active';
        }

        if (in_array('menu-item-has-children', $element->classes)) {
            $liClasses[] = $liClass.'--parent';
        }

        if (in_array('menu__item--btn', $element->classes)) {
            $linkClasses[] = 'menu__item--btn';
        }

        if (in_array("current_page_item", $element->classes)) {
            $liClasses[] = 'menu__item--active';
        }

        $url = $element->url;

        $output .= '<li class="'.implode(' ', $liClasses).'"><a href="'.$url.'" class="'.implode(' ', $linkClasses).'">'.$element->title.'</a>';
    }
}
