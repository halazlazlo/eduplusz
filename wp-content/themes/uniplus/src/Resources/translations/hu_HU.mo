��    \      �     �      �  5   �          "     :  
   B     M     [     o     �     �     �     �     �     �  
   �     �  .   �     	     	     	     /	     >	     F	     Z	     b	     i	  
   �	     �	     �	     �	     �	     �	     �	  	   �	  	   �	  	   �	     �	     �	     
     
     !
     7
     I
     U
     n
     r
     z
     
     �
     �
     �
     �
     �
     �
     �
  3   �
     /     I     `     f     n     }     �     �     �     �     �      �     �               $     6     ?     O     T     c     r     w     }     �     �     �     �     �     �     �     �     �     �     �  4    ?   8     x  !   �     �  	   �     �     �     �                 
   6  	   A     K     Q     ]  >   o     �     �  	   �     �     �     �               !     6     ?     K     R     [     d     w          �     �     �  0   �     �                -     E     ^     p     y     �  %   �     �     �     �     �     �            3     +   R  +   ~     �     �     �     �     �     �       o        {  $   �  $   �     �     �               "     7     E     Z     o     v     }     �     �     �     �     �     �      �     �                                7       D   5                      )      #   $   %   '   B   R   K       *              2   T   4         -      +   U      Z      @   .       ;            Y           ,      [   1   
               ?   9       "   =   F   P   !          8       &   (   <                     E   \   :         /                           0              Q   H   >   S       6   L                 W              G       J       V   3   O          	   I       M   N   X      C   A    A section with the email and a button to contact page Add margin on top? Add some margin on top? Address Advantages Align content An example of costs Background color Background image Button Can't be blank Category Center Color Components Contact form Contact infos in a line, like contact page top Contact line Contact link Contact messages Contact person Content Count of projectors Default E-mail EDU+ malfunction entry Edu+ price Email Example Examples Gallery Green (Apple) Heading Heading 2 Heading 3 Heading 4 Infographics (process) Invalid email address Layout Like discounts Like homepage gallery Like homepage top Malfunction Malfunction form entries Map Message Name New contact email New contact form message New malfunction form entry New survey answer None None (left) Normal price Phone Projector manufacturer and type (eg.: Epson EB-460) Remove padding on bottom? Remove padding on top? Right Savings School address School name Section Section intro Send Sending was successful Sent at Show contact info in this block? Show dots on background image? Slider (Gallery) Slider (Images) Slider (standard) Subtitle Survey messages Text Theme Settings Theme settings Type White Wrapper for elements Yes contact count_of_projectors email malfunction phone projector_type school school_address school_name sent_at Project-Id-Version: Societe 0.1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-07 12:53+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: hu_HU
 Egy szekció az e-mail címmel és egy gomb a kapcsolat oldalra Legyen eltartás (margin) fenn? Legyen eltartás (margin) felül? Cím Előnyök Szöveg elrendezése Példák a költségekről Háttérszín Háttérkép Gomb Mező kitöltése közelező Kategória Középre Szín Komponensek Kapcsolati űrlap Elérhetőségek egy csíkban, mint a kapcsolat oldal tetején Kapcsolati csík Kapcsolat gomb Üzenetek Kontaktszemély Tartalom Projektorok száma Alapértelmezett E-mail cím EDU+ hibabejelentés Edu+ ár E-mail cím Példa Példák Galéria Zöld (Sötétebb) Címsor 2-es címsor 3-as címsor 4-es címsor Infografika (folyamat) Kérjük, adjon meg egy érvényes e-mail címet Oldalfelépítés Pl.: Kedvezmények Pl.: Kezdőoldal galéria Pl.: Kezdőoldal teteje Tapasztalt hibajelenség Hibabejelentések Térkép Üzenet Név Új üzenet az eduplussz.hu oldalról Új üzenet Új hibabejelentés Új kérdőív kitöltés Nincs Balra (alapértelmezett) Normál ár Telefonszám Projektor gyártója és típusa (pl: Epson EB-460) Eltüntessük az eltartást (padding) lenn? Eltüntessük az eltartást (padding) fenn? Jobbra Megtakarítás Iskola címe Iskola neve Szekció Alcím alatti bevezető szöveg Küldés Köszönjük érdeklődését, kollégánk hamarosan felveszi önnel a kapcsolatot a megadott elérhetőségen. Küldés időpontja Mutassunk itt egy kapcsolati infót? Legyenek pöttyök a háttérképen? Képrotáló (galéria) Képrotáló (Ikonok) Képrotáló (alap) Alcím Kérdőív válaszok Folyószöveg Téma beállítások Téma beállítások Típus Fehér Keret Igen Kapcsolattartó Projektorok száma E-mail cím Tapasztalt hibajelenség Telefon Projektor gyártója és típusa Iskola Iskola címe Iskola neve Küldve 