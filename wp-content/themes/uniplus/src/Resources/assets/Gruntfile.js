module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt)

  var configs = require('load-grunt-configs')(grunt);
  configs.config = {
    src: 'src',
    dist: 'dist',
    vendors: {
      js: [
        // jquery
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery.easing/jquery.easing.min.js',

        // maps
        'node_modules/google-maps/lib/Google.min.js',

        // waypoints
        'node_modules/waypoints/lib/noframework.waypoints.js',

        // lightgallery
        'node_modules/lightgallery/dist/js/lightgallery.min.js',
        'node_modules/lg-thumbnail/dist/lg-thumbnail.min.js',

        // swiper
        'node_modules/swiper/dist/js/swiper.min.js'
      ]
    },
  };
  grunt.initConfig(configs);

  grunt.registerTask('default', [
      'build',
      'watch'
  ]);

  grunt.registerTask('build', [
    'clean',
    'copy',

    // css
    'svgstore',
    'sass',
    'postcss',
    'cmq',

    // img
    'imagemin',

    // ts
    'ts',
    'browserify',
    'concat:dev',
    'clean:caches',

    // i18n
    'po2mo'
  ]);

  grunt.registerTask('deploy', [
      'build',
      'concat',
      'cssmin',
      'uglify'
  ]);
}
