declare const $;
declare const GoogleMapsLoader;
declare const baseUrl;

export class MapComponent {
  private apiKey = 'AIzaSyDv3OgnqvRi4F-ITaLaZjWu46tY7CUrQL8';

  constructor() {
    GoogleMapsLoader.KEY = this.apiKey;

    GoogleMapsLoader.load(function(google) {
      const latLng = {lat: -25.363, lng: 131.044};

      $('.map').each(function(index, el) {
        const address = $(this).data('address');

        const map = new google.maps.Map(el, {
          scrollwheel: false,
          zoom: 15,
          center: latLng
        });

        const geocoder = new google.maps.Geocoder();

        geocoder.geocode({
          address: address
        },

        function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            if (status !== google.maps.GeocoderStatus.ZERO_RESULTS) {
              map.setCenter(results[0].geometry.location);

              const _marker = new google.maps.Marker({
                position: results[0].geometry.location,
                map: map,
                title: address,
                icon: baseUrl + '/src/Resources/assets/dist/img/map-pin.png'
              });
            }
          }
        });
      });
    });
  }
}
