declare const $;

import slugify from 'slugify';

// declare const slugify;

export class HeadingComponent {
  constructor() {
    this.updateIds();
  }

  updateIds() {
    $('.h--2').each((i, elem) => {
      const heading = $(elem);
      const title = heading.html();

      const ref = slugify(title).toLowerCase().split('.').join('');

      heading.attr('id', 'js-st-' + ref);
    });
  }
}
