declare const $;

export class ScrollbarService {
  constructor() {
    const hash = window.location.hash.replace('#', '');

    if (hash) {
      this.scrollTo(window.location.hash.replace('#', ''));
    }
  }

  scrollTo(slug) {
    if (slug.includes('#')) {
      slug = slug.substring(1);
    }

    let target = $('#js-st-' + slug);

    if (target.length > 0) {
      const parent = target.parents('.page__section');
      if (parent.length > 0) {
        target = parent;
      }

      const scrollTo = target.offset().top;

      console.log(scrollTo);
       $('html, body').animate(
        {scrollTop: scrollTo},
        300,
        'easeOutCubic',
        () => {
          window.location.hash = slug;
        }
      );
    }
  }
}
