declare const $;
declare const Waypoint;

export class StickyContactComponent {
  constructor() {
    this.init();
  }

  init() {
    const contact = $('.contact-link--sticky');

    if ($('.contact-link__waypoint').length) {
      const waypoint = new Waypoint({
        element: $('.contact-link__waypoint')[0],
        handler: function(direction) {
          if (direction === 'down') {
            contact.addClass('contact-link--fixed');
          } else {
            contact.removeClass('contact-link--fixed');
          }
        }
      });
    }
  }
}
