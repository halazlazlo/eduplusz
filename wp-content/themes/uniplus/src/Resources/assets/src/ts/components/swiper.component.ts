import { ViewportService } from "../services/viewport.service";

declare const $;
declare const Swiper;

export class SwiperComponent {
  private viewportService: ViewportService = new ViewportService();

  constructor() {
    if ($('.js-swiper--std').length) {
      this.initStdSwiper();
    }

    if ($('.js-swiper--img').length) {
      this.initImgSwiper();
    }

    if ($('.js-swiper--gallery-slider').length) {
      this.initGallerySwiper();
    }
  }

  initStdSwiper() {
    const _imgSlider = new Swiper('.js-swiper--std', {
      // Optional parameters
      direction: 'horizontal',

      // lazy
      preloadImages: false,
      lazyLoading: true,
      lazyLoadingInPrevNext: true,

      pagination: '.js-swiper--std .swiper-pagination',
      paginationClickable: true,
      loop: true,
      grabCursor: true,
      autoplay: 5000
    });
  }

  initImgSwiper() {
    let slidesPerView = 3;

    if (this.viewportService.getLayout() === 'xs') {
      slidesPerView = 1;
    } else if (this.viewportService.getLayout() === 'sm') {
      slidesPerView = 2;
    }

    const _imgRotator = new Swiper('.js-swiper--img', {
      // Optional parameters
      direction: 'horizontal',

      // Navigation arrows
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',

      // lazy
      preloadImages: false,
      lazyLoading: true,
      lazyLoadingInPrevNext: true,
      slidesPerView: slidesPerView,
      spaceBetween: 30,
      grabCursor: true
    });
  }

  initGallerySwiper() {
    let slidesPerView = 4;

    if (this.viewportService.getLayout() === 'xs') {
      slidesPerView = 1;
    } else if (this.viewportService.getLayout() === 'sm') {
      slidesPerView = 3;
    }

    new Swiper('.js-swiper--gallery-slider', {
      // Optional parameters
      direction: 'horizontal',

      // Navigation arrows
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',

      pagination: '.swiper-pagination--gallery-slider',
      paginationClickable: true,

      // lazy
      preloadImages: false,
      lazyLoading: true,
      lazyLoadingInPrevNext: true,
      grabCursor: true,
      slidesPerView: slidesPerView,
      spaceBetween: 30
    });
  }
}
