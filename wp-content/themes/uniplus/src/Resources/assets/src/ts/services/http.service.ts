declare const $;
declare const apiUrl;

export class HttpService {
    get(action, params, cb) {
        $.ajax({
          url: apiUrl+'?action='+action,
          type: 'GET',
          dataType: 'json',
          data: params
        })
        .done(response => {
            cb(response);
        });
    }
}
