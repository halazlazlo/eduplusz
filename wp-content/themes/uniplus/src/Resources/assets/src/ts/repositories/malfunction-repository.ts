import { HttpService } from "../services/http.service";

declare const apiUrl;

export class MalfunctionRepository {
    private httpService;

    constructor(httpService: HttpService) {
        this.httpService = httpService;
    }

    findBySchoolname(schoolName, callback) {
        this.httpService.get(
            'find_one_by_school_name',
            {schoolName: schoolName},
            response => {callback(response);}
        );
    }
}
