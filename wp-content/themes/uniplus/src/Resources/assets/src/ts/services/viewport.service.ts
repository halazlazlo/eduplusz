export class ViewportService {
  public getLayout() {
    return this.replaceAll(window.getComputedStyle(document.body, ':before')
      .getPropertyValue('content'), '"', '');
  }

  public getViewport() {
    let e: any = window;
    let a: any = 'inner';

    if (!('innerWidth' in window)) {
      a = 'client';
      e = document.documentElement || document.body;
    }

    return {width: e[a + 'Width'], height: e[a + 'Height']};
  }

  public isMobile() {
    const layout = this.getLayout();

    return layout === 'xs' || layout === 'sm' || layout === 'md';
  }

  private escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|[\]/\\])/g, '\\$1');
  };

  private replaceAll(string, find, replace) {
    return string.replace(new RegExp(this.escapeRegExp(find), 'g'), replace);
  };
}
