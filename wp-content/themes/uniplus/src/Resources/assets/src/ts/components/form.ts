declare const $;

export class FormComponent {
  private form;
  private submitBtn;
  private loader;
  private msg;

  constructor() {
    this.init();
  }

  init() {
    $('.form').on('submit', (event: Event) => {
      event.preventDefault();

      this.form = $(event.target);

      this.submitBtn = this.form.find('.btn');
      this.loader = this.form.find('.form__loader');
      this.msg = this.form.find('.form__msg');

      this.submit();
    });
  }

  submit() {
    // remove errors
    this.form.find('.form__errors').remove();

    // hide send
    this.submitBtn.addClass('hide');

    // appear loader
    this.loader.removeClass('hide');

    $.ajax({
      url: this.form.attr('action'),
      type: 'POST',
      dataType: 'json',
      data: this.form.serialize()
    })
    .done(response => {
      this.loader.addClass('hide');

      if (response.success) {
        this.msg.removeClass('hide');

        this.form.find('.form__fields').slideUp(300);
      } else {
        // add errors
        this.showErrors(response.errors);

        // scroll to first error
        const fisrtError = this.form.find('.form__errors').first();
        $('html, body').animate({
          scrollTop: fisrtError.parents('.form__group').offset().top - 15
        }, 300);

        // show submit btn
        this.submitBtn.removeClass('hide');
      }
    });
  }

  showErrors(errors) {
    Object.keys(errors).forEach(id => {
      const ret = $('<ul class="form__errors"></ul>');

      const messages = errors[id];
      messages.forEach(message => {
        ret.append($('<li class="form__error">' + message + '</li>'));
      });

      $('#' + id).parents('.form__group').addClass('form__group--error').append(ret);
    });
  }
}
