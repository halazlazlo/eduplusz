declare const lightGallery;
declare const $;

export class GalleryComponent {
  constructor() {
    this.init();
  }

  init() {
    $('.js-lightgallery').lightGallery({
      thumbnail: true,
      showThumbByDefault: false
    });
  }
}
