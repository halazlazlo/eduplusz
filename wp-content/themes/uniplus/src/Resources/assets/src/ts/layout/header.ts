import { ViewportService } from '../services/viewport.service';

declare const $;

export class Header {
  private viewportService: ViewportService;

  constructor() {
    this.viewportService = new ViewportService();

    this.init();
  }

  init() {
    const activeLink = $('.menu > .menu__item > .menu__link--active');

    if (activeLink.length) {
      const activeLinkLeft = activeLink.offset().left;
      const menu = $('.menu');
      const menuLeft = menu.offset().left;

      const bgLineLeft = Math.abs(menuLeft - activeLinkLeft);

      $('.header__menu-bg').css('width', bgLineLeft);

      // menu
      $('.menu__item--parent > .menu__link').on('click', function(event) {
        const submenu = $(this).parents('.menu__item').find('.menu__submenu');

        if (this.viewportService.isMobile() && !submenu.hasClass('menu__submenu--open')) {
          event.preventDefault();
          submenu.addClass('menu__submenu--open');
        }
      });
    }
  }
}
