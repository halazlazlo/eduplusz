import { MalfunctionRepository } from "../repositories/malfunction-repository";
import { HttpService } from "../services/http.service";

declare const $;

export class MalfunctionForm {
    private httpService = new HttpService();

    init() {
        $('#malfunction_form_school_name').focusout((event) => {
            const schoolName = $(event.target).val();
            const malfunctionRepository = new MalfunctionRepository(this.httpService);

            if (schoolName) {
                malfunctionRepository.findBySchoolname(schoolName, school => {
                    if (school) {
                        Object.keys(school).forEach(key => {
                            $(`#malfunction_form_${key}`).val(school[key]);
                        });
                    }
                });
            }
        });
    }
}
