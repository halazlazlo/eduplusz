import { FormComponent } from './components/form';
import { GalleryComponent } from './components/gallery.component';
import { HeadingComponent } from './components/heading.component';
import { MalfunctionForm } from './components/malfunction-form';
import { MapComponent } from './components/map.component';
import { StickyContactComponent } from './components/sticky-contact.component';
import { SwiperComponent } from './components/swiper.component';
import { ScrollbarService } from './services/scrollbar.service';

declare const $;

$(document).ready(() => {
  // layout
  // const header = new Header();

  // components
  const form = new FormComponent();
  const heading = new HeadingComponent();
  const map = new MapComponent();
  const stickyContact = new StickyContactComponent();
  const gallery = new GalleryComponent();
  const swiper = new SwiperComponent();

  const malfunctionForm = new MalfunctionForm();
  malfunctionForm.init();

  // scrollto
  const scrollbarService = new ScrollbarService();
});
