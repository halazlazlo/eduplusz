module.exports = {
  dist: {
    src: [
      "<%= config.dist %>",
      "dist"
    ]
  },
  caches: {
    src: [
      '.tscache',
      '<%= config.dist %>/ts-compiled',
    ]
  }
};
