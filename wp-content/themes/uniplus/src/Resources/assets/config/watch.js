module.exports = {
	ts: {
		files: [
			'<%= config.src %>/ts/**/*',
		],
		tasks: [
			'ts',
			'browserify',
			'concat:dev',
			'clean:caches'
		],
		options: {
			spawn: false
		}
	},
	css: {
		files: [
			'<%= config.src %>/scss/**/*'
		],
		tasks: [
			'sass',
			'postcss',
			'cmq',
		],
		options: {
			spawn: false
		}
	},
	imgs: {
		files: [
			'<%= config.src %>/img/**/*'
		],
		tasks: [
			'imagemin'
		],
		options: {
			spawn: false
		}
	},
	svgs: {
		files: ['<%= config.src %>/svg/**/*'],
		tasks: ['imagemin:svg', 'svgstore'],
		options: {
			spawn: false
		}
	},
	fonts: {
		files: ['<%= config.src %>/fonts/**/*'],
		tasks: ['copy:custom_fonts'],
		options: {
			spawn: false
		}
	},
	languages: {
		files: ['../translations/*.po'],
		tasks: ['po2mo'],
		options: {
			spawn: false
		}
	}
};
