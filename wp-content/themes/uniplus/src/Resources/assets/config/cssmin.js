module.exports = {
  target: {
    options: {
      keepSpecialComments: false
    },
    files: [{
      expand: true,
      cwd: '<%= config.dist %>/css',
      src: ['*.css'],
      dest: '<%= config.dist %>/css',
      ext: '.css'
    }]
  }
}
