module.exports = {
    options: {
        config: '.scsslint.yml'
    },
    src: ['<%= config.src %>/scss/**/*']
}
