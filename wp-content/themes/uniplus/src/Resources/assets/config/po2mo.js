module.exports = {
  hu: {
    src: '../translations/hu_HU.po',
    dest: '../translations/hu_HU.mo',
  },
  en: {
    src: '../translations/en_US.po',
    dest: '../translations/en_US.mo',
  },
}
