module.exports = {
  custom_fonts: {
    expand: true,
    cwd: '<%= config.src %>/fonts',
    src: '**',
    dest: '<%= config.dist %>/fonts'
  },
  open_sans_eb: {
    expand: true,
    cwd: 'node_modules/npm-font-open-sans/fonts/ExtraBold',
    src: '**',
    dest: '<%= config.dist %>/fonts'
  },
  open_sans_b: {
    expand: true,
    cwd: 'node_modules/npm-font-open-sans/fonts/Bold',
    src: '**',
    dest: '<%= config.dist %>/fonts'
  },
  open_sans_r: {
    expand: true,
    cwd: 'node_modules/npm-font-open-sans/fonts/Regular',
    src: '**',
    dest: '<%= config.dist %>/fonts'
  },
  lg: {
    expand: true,
    cwd: 'node_modules/lightgallery/dist/fonts',
    src: '**',
    dest: '<%= config.dist %>/fonts'
  }
};
