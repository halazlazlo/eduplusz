<?php

// https://bitbucket.org/digital_capital/wordpress-autoloader/wiki/browse/

$parameters = [
    'language_textdomain' => get_template(),
    'available_menu_pages' => [
        'index.php',                    // dashboard
        'edit.php',                  // posts
        'edit.php?post_type=page',   // pages
        'themes.php',                // appearance
        'plugins.php',               // plugins
        'users.php',                 // users
        'options-general.php',        // settings
        'upload.php',                // media
    ],
    'screen_options' => [
        'page' => [
            'submitdiv', //– Date, status, and update/save metabox
            'title', // content title
            'editor', // content editor
            'pageparentdiv', //– Attributes metabox
            'page-attributes',
        ]
    ],
    'theme_settings' => [
        'menu_label' => __('Theme settings', get_template()),
        'form_fields' => [
            'copyright' => [
                'label'       => __('Copyright text', get_template()),
                'type'        => 'text',
                'placeholder' => '',
            ],
            'facebook' => [
                'label'       => 'Facebook',
                'type'        => 'text',
                'placeholder' => '',
            ],
            'address' => [
                'label'       => __('Address', get_template()),
                'type'        => 'text',
                'placeholder' => '',
            ],
            'phone' => [
                'label'       => __('Phone', get_template()),
                'type'        => 'text',
                'placeholder' => '',
            ],
            'email' => [
                'label'       => __('E-mail', get_template()),
                'type'        => 'text',
                'placeholder' => '',
            ]
        ]
    ]
];
