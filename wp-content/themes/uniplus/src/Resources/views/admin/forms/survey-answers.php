<h1>
    <?php echo __('Survey answers', get_template()); ?>
</h1><br>

<table class="wp-list-table widefat fixed striped pages">
    <thead>
        <tr>
            <?php foreach ($form['fields'] as $key => $field): ?>
                <?php $label = (isset($field['label'])) ? $field['label'] : __($key, 'uniplus'); ?>
                <td class="manage-column column-name column-primary"><?php echo $label; ?></td>
            <?php endforeach ?>
        </tr>
    </thead>
    <tbody id="the-list">
        <?php foreach($contact_messages as $message) : ?>
        <tr class="iedit author-self level-0 post-150 type-page status-publish hentry">
            <?php foreach ($form['fields'] as $key => $field): ?>
                <td class="manage-column column-name column-primary">
                    <?php echo $message[$key]; ?>
                </td>
            <?php endforeach ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php if (isset($pagination) && $pagination) : ?>
    <div class="tablenav bottom">
        <div class="tablenav-pages">
            <?php echo $pagination; ?>
        </div>
    </div>
<?php endif; ?>

<p class="submit">
    <a href="/wp-admin/admin-ajax.php?action=survey_form_export" class="button button-primary">
        Letöltés .xls formátumban
    </a>
</p>
