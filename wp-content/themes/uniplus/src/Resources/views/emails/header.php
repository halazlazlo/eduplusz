<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo __('New contact form message', 'uniplus'); ?></title>
        <meta http-equiv="imagetoolbar" content="no" />
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>

    <body style="border:0;margin:0">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
        <tr>
        <td>
        <table  align="center"
                bgcolor="#ffffff"
                border="0"
                cellspacing="0"
                cellpadding="0"
                style="border-collapse: collapse;"
                width="650">

            <tr>
                <td style="padding: 32px" valign="top">

                    <table>
                        <tr>
                            <td style="padding: 20px 20px 0 20px">
                                <table  align="center"
                                        border="0"
                                        cellspacing="0"
                                        cellpadding="0"
                                        style="border-collapse: collapse;"
                                        width="100%">
