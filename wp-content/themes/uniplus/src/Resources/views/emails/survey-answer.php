<?php include 'header.php'; ?>

    <tr>
        <td>
            <h1><?php echo __('New survey answer', 'uniplus'); ?></h1>
        </td>
    </tr>
    <?php foreach ($form['fields'] as $key => $field): ?>
        <tr>
            <td>
                <?php echo (isset($field['label'])) ? $field['label'] : __($key, 'uniplus'); ?>
            </td>
            <td>
                <?php if ($survey_answer[$key]) : ?>
                    <?php echo $survey_answer[$key]; ?>
                <?php else: ?>
                    -
                <?php endif ?>
            </td>
        </tr>
    <?php endforeach ?>

<?php include 'footer.php'; ?>
