<?php include 'header.php'; ?>

<tr>
    <td>
        Tisztelt Partnerünk!<br><br>

        Köszönjük szépen a bejelentését!<br>
        Az edu+ program csapata hamarosan felveszi Önnel a kapcsolatot a hibás projektor(ok) elszállítását illetően.<br><br>

        Üdvözlettel:<br><br>

        Kada Mihály<br>
        ügyvezető<br>
        <strong>Projektorszerviz - Dielomax Kft.</strong><br>
        Iroda, szerviz: H-2100 Gödöllő, Széchenyi u. 20<br>
        Mobil: +36 20 9 44 33 66<br>
        Szerviz: +36 28 41 08 08<br>
        <a href="http://eduplussz.hu">www.eduplussz.hu</a>
    </td>
</tr>

<?php include 'footer.php'; ?>
