<?php
    $options = [
        'title' => __('New malfunction form entry', 'uniplus')
    ];
?>

<?php include 'header.php'; ?>

<tr>
    <td>
        <h1><?php echo $options['title']; ?></h1>
    </td>
</tr>

<?php foreach ($form['fields'] as $key => $field): ?>
    <tr>
        <td>
            <?php if ($field['label']) : ?>
                <?php echo $field['label']; ?>
            <?php elseif ($field['label_hidden']) : ?>
                <?php echo $field['label_hidden']; ?>
            <?php elseif ($field['placeholder']) : ?>
                <?php echo $field['placeholder']; ?>
            <?php endif ?>
        </td>
        <td>
            <?php if ($entityToSave[$key]) : ?>
                <?php echo $entityToSave[$key]; ?>
            <?php else: ?>
                -
            <?php endif ?>
        </td>
    </tr>
<?php endforeach ?>

<?php include 'footer.php'; ?>
