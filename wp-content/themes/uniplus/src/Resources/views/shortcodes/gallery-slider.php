<?php
    global $detect;

    // get timeline children
    $children = [];
    $childrenStr = explode(']', $content);
    array_pop($childrenStr);

    foreach ($childrenStr as $childStr) {

        // put child together
        $child        = [];
        $childStr     = $childStr . ']';
        $childContent = do_shortcode($childStr);
        $childAtts = explode('#####', $childContent);

        foreach ($childAtts as $att) {
            $attArr = explode('##', $att);

            if (sizeof($attArr) > 1) {
                list($k, $v) = $attArr;

                $child[$k] = $v;
            }
        }

        $children[] = $child;
    }
?>

<div class="slider slider--gallery js-swiper--gallery-slider">
    <div class="swiper-wrapper">
        <?php foreach ($children as $slide): ?>
            <?php
                $img = null;

                if (isset($slide['img'])) {
                    $img = wp_get_attachment_image_src($slide['img'], 'md');

                    $slide['img'] = $img[0];
                }

                $link = vc_build_link($slide['link']);
                $slide['link'] = $link;
            ?>

            <div class="swiper-slide slide">
                <?php if ($slide['img']): ?>
                    <figure class="slide__img img img--bg" style="background-image: url('<?php echo $slide['img']; ?>')"></figure>
                <?php endif ?>

                <div class="slide__caption">
                    <?php if (isset($slide['title'])): ?>
                        <h3 class="h slide__title">
                            <?php echo $slide['title'] ?>
                        </h3>
                    <?php endif ?>

                    <?php if (isset($slide['link'])): ?>
                        <span class="slide__link">
                            <a href="<?php echo $slide['link']['url']; ?>" target="<?php echo $slide['link']['target']; ?>" class="link">
                                olvass tovább
                            </a>

                            <svg viewBox="0 0 100 100" class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-plus"></use>
                            </svg>

                            <svg viewBox="0 0 100 100" class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-plus"></use>
                            </svg>

                            <svg viewBox="0 0 100 100" class="icon">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-plus"></use>
                            </svg>
                        </span>
                    <?php endif ?>
                </div>
            </div>
        <?php endforeach ?>
    </div>

    <!-- If we need navigation buttons -->
    <div class="swiper__btn swiper__btn--prev swiper-button-prev"></div>
    <div class="swiper__btn swiper__btn--next swiper-button-next"></div>

    <div class="swiper-pagination swiper-pagination--gallery-slider"></div>
</div>
