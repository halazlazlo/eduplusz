<?php
    $imgs     = [];
    $imgIds = isset($atts['img_ids']) ? explode(',', $atts['img_ids']) : [];

    foreach ($imgIds as $imgId) {
        $img = wp_get_attachment_image_src($imgId, 'sm');

        if (isset($img[0])) {
            $imgs[] = $img[0];
        }
    }

    $slider = [
        'imgs' => $imgs
    ];
?>

<div class="slider slider--gallery js-swiper--gallery-slider">
    <div class="swiper-wrapper">
        <?php foreach ($slider['imgs'] as $img): ?>

            <div class="swiper-slide slider__slide">
                <a href="/galeria">
                    <figure class="slider__slide-img img img--bg" style="background-image: url('<?php echo $img; ?>')"></figure>
                </a>
            </div>
        <?php endforeach ?>
    </div>

    <!-- If we need navigation buttons -->
    <div class="swiper__btn swiper__btn--prev swiper-button-prev"></div>
    <div class="swiper__btn swiper__btn--next swiper-button-next"></div>

    <div class="swiper-pagination swiper-pagination--gallery-slider"></div>
</div>
