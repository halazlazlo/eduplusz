<div class="contact-link">
    <span class="contact-link__link">
        <?php echo get_option('email'); ?>
    </span>

    <a href="/kapcsolat" class="contact-link__btn">
        Írjon nekünk
    </a>
</div>
