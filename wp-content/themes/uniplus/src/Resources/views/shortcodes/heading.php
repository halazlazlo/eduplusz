<?php
    $heading = [
        'txt'   => isset($atts['txt']) ? $atts['txt'] : null,
        'class' => isset($atts['class']) ? $atts['class'] : '2',
        'align' => isset($atts['align']) ? $atts['align'] : null
    ];

    $classes = ['h', 'h--'.$heading['class']];

    if ($heading['align']) {
        $classes[] = 'h--'.$heading['align'];
    }
 ?>

<h<?php echo $heading['class']; ?> class="<?php echo implode(' ', $classes); ?>">
    <?php echo $heading['txt']; ?>
</h<?php echo $heading['class']; ?>>
