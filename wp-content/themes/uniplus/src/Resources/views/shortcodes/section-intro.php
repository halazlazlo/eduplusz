<?php
    $sectionIntro = [
        'content' => apply_filters('the_content', wpautop($content))
    ];
?>

<div class="page__section-intro">
    <?php echo $sectionIntro['content']; ?>
</div>
