<?php
    $href = vc_build_link($atts['href']);

    $btn = [
        'url'    => isset($href['url']) ? $href['url'] : null,
        'title'  => isset($href['title']) ? $href['title'] : null,
        'target' => isset($href['target']) ? $href['target'] : '_self',
        'rel'    => isset($href['rel']) ? $href['rel'] : null
    ];
?>

<a href="<?php echo $btn['url']; ?>" <?php if ($btn['target']): ?>target="<?php echo $btn['target']; ?>"<?php endif ?> class="btn btn--std">
    <?php echo $btn['title']; ?>
</a>
