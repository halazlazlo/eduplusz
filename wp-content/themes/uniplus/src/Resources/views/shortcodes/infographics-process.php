<?php

    $ig = [
        'col_left_title' => isset($atts['col_left_title']) ? $atts['col_left_title']: null,
        'col_left_desc' => isset($atts['col_left_desc']) ? $atts['col_left_desc']: null,
        'col_mid_title' => isset($atts['col_mid_title']) ? $atts['col_mid_title']: null,
        'col_mid_left_desc' => isset($atts['col_mid_left_desc']) ? $atts['col_mid_left_desc']: null,
        'col_mid_mid_desc' => isset($atts['col_mid_mid_desc']) ? $atts['col_mid_mid_desc']: null,
        'col_mid_right_desc' => isset($atts['col_mid_right_desc']) ? $atts['col_mid_right_desc']: null,
        'col_right_title' => isset($atts['col_right_title']) ? $atts['col_right_title']: null,
        'col_right_desc' => isset($atts['col_right_desc']) ? $atts['col_right_desc']: null
    ];
?>

<div class="ig ig--process">
    <div class="ig__col ig__col--left">
        <div class="ig__head">
            <h3 class="ig__title">
                <?php echo $ig['col_left_title']; ?>
            </h3>
        </div>

        <img class="ig__img" src="<?php echo ASSET_URI.'/img/infographics-process-1.png'; ?>" alt="Megrendelő feladata 1.">

        <div class="ig__desc">
            <?php echo $ig['col_left_desc']; ?>
        </div>
    </div>

    <div class="ig__col ig__col--mid">
        <div class="ig__head">
            <h3 class="ig__title">
                <?php echo $ig['col_mid_title']; ?>
            </h3>
        </div>

        <div class="ig__col-block">
            <img class="ig__img" src="<?php echo ASSET_URI.'/img/infographics-process-2.png'; ?>" alt="Megrendelő feladata 1.">

            <div class="ig__desc">
                <?php echo $ig['col_mid_left_desc']; ?>
            </div>
        </div>

        <div class="ig__col-block ig__col-block--mid">
            <img class="ig__img" src="<?php echo ASSET_URI.'/img/infographics-process-3.png'; ?>" alt="Megrendelő feladata 1.">

            <div class="ig__desc">
                <?php echo $ig['col_mid_mid_desc']; ?>
            </div>
        </div>

        <div class="ig__col-block">
            <img class="ig__img" src="<?php echo ASSET_URI.'/img/infographics-process-4.png'; ?>" alt="Megrendelő feladata 1.">

            <div class="ig__desc">
                <?php echo $ig['col_mid_right_desc']; ?>
            </div>
        </div>
    </div>

    <div class="ig__col ig__col--right">
        <div class="ig__head">
            <h3 class="ig__title">
                <?php echo $ig['col_right_title']; ?>
            </h3>
        </div>

        <img class="ig__img" src="<?php echo ASSET_URI.'/img/infographics-process-5.png'; ?>" alt="Megrendelő feladata 2.">

        <div class="ig__desc">
            <?php echo $ig['col_right_desc']; ?>
        </div>
    </div>
</div>
