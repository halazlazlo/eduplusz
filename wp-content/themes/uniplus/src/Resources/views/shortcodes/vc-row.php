<?php

    $bgImgId = isset($atts['bg_img']) ? $atts['bg_img'] : null;
    $bgImg = null;

    if ($bgImgId) {
        $bgImgSrc = wp_get_attachment_image_src($bgImgId, 'full');

        $bgImg = $bgImgSrc[0];
    }

    $section = [
        'bg'                => (isset($atts['bg'])) ? $atts['bg'] : null,
        'bg_img'            => $bgImg,
        'bg_img_overlay'    => (isset($atts['bg_img_overlay'])) ? ($atts['bg_img_overlay'] === '1') : false,
        'show_contact_link' => (isset($atts['show_contact_link'])) ? ($atts['show_contact_link'] === '1') : false,
        'mt'                => (isset($atts['mt'])) ? ($atts['mt'] === '1') : false,
        'bt'                => (isset($atts['bt'])) ? ($atts['bt'] === '1') : false,
        'rm_pt'             => (isset($atts['rm_pt'])) ? ($atts['rm_pt'] === '1') : false,
        'rm_pb'             => (isset($atts['rm_pb'])) ? ($atts['rm_pb'] === '1') : false,
        'content'           => wpb_js_remove_wpautop($content)
    ];

    $classes = ['page__section'];

    if ($section['bg']) {
        $classes[] = 'page__section--bg-'.$section['bg'];
    }

    if ($section['mt']) {
        $classes[] = 'page__section--mt';
    }

    if ($section['rm_pt']) {
        $classes[] = 'page__section--rm-pt';
    }

    if ($section['bt']) {
        $classes[] = 'page__section--bt';
    }

    if ($section['rm_pb']) {
        $classes[] = 'page__section--rm-pb';
    }

    if ($section['bg_img']) {
        $classes[] = 'page__section--bg-img';
    }

    if ($section['bg_img_overlay']) {
        $classes[] = 'page__section--bg-img-overlay';
    }
?>

<section class="<?php echo implode(' ', $classes); ?>" <?php if ($section['bg_img']): ?>
    style="background-image: url('<?php echo $section['bg_img']; ?>');"
<?php endif ?>>
    <?php echo $section['content']; ?>

    <?php if ($section['show_contact_link']): ?>
        <div class="contact-link__waypoint"></div>
        <div class="contact-link contact-link--sticky">
            <span class="contact-link__link">
                <?php echo get_option('email'); ?>
            </span>
            <a href="/kapcsolat" class="contact-link__btn">
                Írjon nekünk
            </a>
        </div>
    <?php endif ?>
</section>
