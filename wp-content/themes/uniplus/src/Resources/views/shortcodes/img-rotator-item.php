<?php
    $ret = '';

    if ($content) {
        $ret = 'content##'.apply_filters('the_content', wpautop($content));
    }

    $i = 0;
    foreach ($atts as $key => $value) {
        if ($content || $i > 0) {
            $ret .= '#####';
        }

        $ret .= $key.'##'.$value;

        $i++;
    }

    echo $ret;
 ?>
