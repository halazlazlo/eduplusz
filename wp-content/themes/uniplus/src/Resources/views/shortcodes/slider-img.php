<?php
    // get timeline children
    $children = [];
    $childrenStr = explode('[/slider_img_item]', $content);
    array_pop($childrenStr);

    foreach ($childrenStr as $childStr) {

        // put child together
        $child        = [];
        $childStr     = $childStr . '[/slider_img_item]';
        $childContent = do_shortcode($childStr);
        $childAtts = explode('#####', $childContent);

        foreach ($childAtts as $att) {
            $attArr = explode('##', $att);

            if (sizeof($attArr) > 1) {
                list($k, $v) = $attArr;

                $child[$k] = $v;
            }
        }

        $children[] = $child;
    }

    $slider = [
        'mt'    => (isset($atts['mt'])) ? ($atts['mt'] === '1') : false,
    ];

    $classes = [
        'slider',
        'slider--img',
        'js-swiper--img'
    ];

    if ($slider['mt']) {
        $classes[] = 'slider--mt';
    }
?>

<div class="<?php echo implode(' ', $classes); ?>">
    <div class="swiper-wrapper">
        <?php foreach ($children as $child): ?>
            <div class="swiper-slide slider__slide">
                <?php
                    $img = null;

                    if (isset($child['img'])) {
                        $img = wp_get_attachment_image_src($child['img'], 'sm');

                        $child['img'] = $img[0];
                    }
                ?>

                <?php if (isset($child['img'])): ?>
                    <div class="slider__slide-img">
                        <img src="<?php echo $child['img']; ?>" alt="">
                    </div>
                <?php endif ?>

                <?php if (isset($child['title'])): ?>
                    <h3 class="h h--3 slider__slide-title">
                        <?php echo $child['title']; ?>
                    </h3>
                <?php endif ?>

                <?php if (isset($child['content'])): ?>
                    <div class="slider__slide-content">
                        <?php echo $child['content']; ?>
                    </div>
                <?php endif ?>
            </div>
        <?php endforeach ?>
    </div>

    <!-- If we need navigation buttons -->
    <div class="swiper__btn swiper__btn--prev swiper-button-prev"></div>
    <div class="swiper__btn swiper__btn--next swiper-button-next"></div>
</div>
