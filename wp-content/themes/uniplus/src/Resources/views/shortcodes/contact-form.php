<?php
    global $app;

    $formUtil = $app->getUtil('form');
    $form = $app->getForm('contact');
?>

<?php $formUtil->start($form); ?>
    <?php $formUtil->widget($form, 'sent_at'); ?>

    <div class="form__fields">
        <div class="row">
            <div class="col-sm-6">
                <?php $formUtil->widget($form, 'name'); ?>
            </div>

            <div class="col-sm-6">
                <?php $formUtil->widget($form, 'email'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php $formUtil->widget($form, 'message'); ?>
            </div>
        </div>

        <div class="form__footer">
            <svg viewBox="0 0 100 100" class="form__loader hide icon">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-spin"></use>
            </svg>

            <button class="btn btn--std">
                <?php echo __('Send', 'uniplus'); ?>
            </button>
        </div>
    </div>

    <div class="form__msg hide form__msg--success">
        <?php echo __('Sending was successful', 'uniplus'); ?>
    </div>
<?php $formUtil->end($form); ?>
