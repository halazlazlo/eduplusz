<?php
    $txt = [
        'color' => isset($atts['color']) ? $atts['color'] : null
    ];

    $classes = ['txt'];

    if ($txt['color']) {
        $classes[] = 'txt--'.$txt['color'];
    }
?>

<div class="<?php echo implode(' ', $classes); ?>">
    <?php echo apply_filters('the_content', wpautop($content)); ?>
</div>
