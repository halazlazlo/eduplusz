<?php
    $slider = [
        'imgs' => isset($atts['imgs']) ? $atts['imgs'] : null
    ];

    $imgs     = [];
    $imgIDStr = isset($atts['imgs']) ? $atts['imgs'] : null;
    $imgids   = explode(',', $imgIDStr);

    foreach ($imgids as $imgId) {
        $misc = get_post($imgId);
        $img = wp_get_attachment_image_src($imgId, 'full');

        $imgNew = [
            'src' => $img[0],
            'caption' => $misc->post_excerpt
        ];

        $imgs[] = $imgNew;
    }
?>

<div class="img-slider js-swiper--img-slider">
    <div class="swiper-wrapper">
        <?php foreach ($imgs as $img): ?>
            <div class="swiper-slide">
                <div class="img-slider__item">
                    <figure class="img-slider__img img img--bg" style="background-image: url('<?php echo $img['src']; ?>')"></figure>
                    <div class="img-slider__caption">
                        <?php echo $img['caption']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>

    <div class="swiper-pagination swiper-pagination--img-slider"></div>
</div>
