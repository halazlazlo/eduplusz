<div class="contact-line">
    <ul class="list contact-line__list">
        <li class="contact-line__list-item">
            <a href="mailto:<?php echo get_option('email'); ?>" target="blank" class="contact-line__link"><?php echo get_option('email'); ?></a>
        </li>
        <li class="contact-line__list-item">
            <?php echo get_option('address'); ?>
        </li>
        <li class="contact-line__list-item">
            <a href="tel:<?php echo get_option('phone'); ?>">
                <?php echo get_option('phone'); ?>
            </a>
        </li>
    </ul>
</div>
