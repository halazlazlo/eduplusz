<?php
    global $app;

    $formUtil = $app->getUtil('form');
    $form = $app->getForm('survey');
?>

<?php $formUtil->start($form); ?>
    <?php $formUtil->widget($form, 'sent_at'); ?>

    <div class="form__fields">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <?php $formUtil->widget($form, 'school'); ?>
                </div>

                <div class="col-sm-6">
                    <?php $formUtil->widget($form, 'count_of_projectors'); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <?php $formUtil->widget($form, 'contact'); ?>
                </div>

                <div class="col-sm-6">
                    <?php $formUtil->widget($form, 'phone'); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?php $formUtil->widget($form, 'email'); ?>
                </div>
            </div>

            <div class="form__footer">
                <svg viewBox="0 0 100 100" class="form__loader hide icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-spin"></use>
                </svg>

                <button class="btn btn--std">
                    <?php echo __('Send', 'uniplus'); ?>
                </button>
            </div>
        </div>
    </div>

    <div class="form__msg hide form__msg--success">
        Az edu+ program köszöni szépen a segítségét! Amennyiben problémája adódik bármelyik projektorával, kérem jelezze az <a class="link link--std" href="mailto:info@eduplussz.hu" target="_blank">info@eduplussz.hu</a> címen.
    </div>
<?php $formUtil->end($form); ?>
