<div class="advantages">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-7 col-md-offset-3 col-lg-5 col-lg-offset-4">
                <ul class="list advantages__list advantages__list--top">
                    <li class="advantages__list-item">
                        nincs szükség jelentkezésre
                    </li>
                    <li class="advantages__list-item">
                        nincs külön regisztráció
                    </li>
                    <li class="advantages__list-item">
                        nem igényel plusz adminisztrációt
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="advantages__list--bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <ul class="list advantages__list">
                        <li class="advantages__list-item">
                            Minden oktatási intézmény automatikusan részesülhet a kedvezményekből
                        </li>
                        <li class="advantages__list-item">
                            Elég csak írásban az <a href="mailto:<?php echo get_option('email') ?>" target="blank" class="link link--std"><?php echo get_option('email') ?></a> címre bejelenteni a hibát
                        </li>
                        <li class="advantages__list-item">
                            Megadni a kontaktszemélyt, a postacímet és egy telefonos elérhetőséget
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
