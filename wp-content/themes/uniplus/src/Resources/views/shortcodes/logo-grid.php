<?php
    $imgs     = [];
    $imgIds = isset($atts['img_ids']) ? explode(',', $atts['img_ids']) : [];

    foreach ($imgIds as $imgId) {
        $img = wp_get_attachment_image_src($imgId, 'sm');

        if (isset($img[0])) {
            $imgs[] = $img[0];
        }
    }
?>

<div class="logo-grid">
    <?php foreach ($imgs as $img): ?>
        <div class="logo-grid__item">
            <img src="<?php echo $img; ?>" alt="">
        </div>
    <?php endforeach ?>
</div>
