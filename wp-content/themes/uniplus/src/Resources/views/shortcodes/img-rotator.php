<?php
    // get timeline children
    $children = [];
    $childrenStr = explode('[/img_rotator_item]', $content);
    array_pop($childrenStr);

    foreach ($childrenStr as $childStr) {

        // put child together
        $child        = [];
        $childStr     = $childStr . '[/img_rotator_item]';
        $childContent = do_shortcode($childStr);
        $childAtts = explode('#####', $childContent);

        foreach ($childAtts as $att) {
            $attArr = explode('##', $att);

            if (sizeof($attArr) > 1) {
                list($k, $v) = $attArr;

                $child[$k] = $v;
            }
        }

        $children[] = $child;
    }
?>

<div class="img-rotator js-swiper--img-rotator">
    <div class="swiper-wrapper">
        <?php foreach ($children as $child): ?>
            <div class="swiper-slide">
                <?php
                    $img = null;

                    if (isset($child['img'])) {
                        $img = wp_get_attachment_image_src($child['img'], 'sm');

                        $child['img'] = $img[0];
                    }
                ?>
                <div class="img-rotator__item">
                    <?php if (isset($child['img'])): ?>
                        <div class="img-rotator__img">
                            <img src="<?php echo $child['img']; ?>" alt="">
                        </div>
                    <?php endif ?>

                    <?php if (isset($child['title'])): ?>
                        <h3 class="h h--3">
                            <?php echo $child['title']; ?>
                        </h3>
                    <?php endif ?>

                    <?php if (isset($child['content'])): ?>
                        <div class="img-rotator__content">
                            <?php echo $child['content']; ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        <?php endforeach ?>
    </div>

    <!-- If we need navigation buttons -->
    <div class="swiper__btn swiper__btn--prev swiper-button-prev"></div>
    <div class="swiper__btn swiper__btn--next swiper-button-next"></div>
</div>
