<?php
    $imgs = [];
    $imgIds = isset($atts['img_ids']) ? explode(',', $atts['img_ids']) : [];

    foreach ($imgIds as $imgId) {
        $imgMeta = get_post($imgId);

        $img = [
            'thumb'   => wp_get_attachment_image_src($imgId, 'sm')[0],
            'lg'      => wp_get_attachment_image_src($imgId, 'lg')[0],
            'caption' => $imgMeta->post_excerpt
        ];

        $imgs[] = $img;
    }

    $gallery = [
        'imgs' => $imgs
    ];
 ?>

<div class="gallery gallery--std js-lightgallery">
    <?php foreach ($gallery['imgs'] as $img): ?>
        <a href="<?php echo $img['lg']; ?>" class="gallery__item" title="<?php echo $img['caption']; ?>">
            <img src="<?php echo $img['thumb']; ?>">
            <figure class="img img--bg gallery__item-img" style="background-image: url('<?php echo $img['thumb']; ?>')">

                <?php if ($img['caption']): ?>
                    <div class="gallery__item-caption">
                        <?php echo $img['caption']; ?>
                    </div>
                <?php endif ?>
            </figure>
        </a>
    <?php endforeach ?>
</div>
