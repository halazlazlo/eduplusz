<?php
    $slider = [
        'imgs' => isset($atts['imgs']) ? $atts['imgs'] : null
    ];

    $imgs     = [];
    $imgIDStr = isset($atts['imgs']) ? $atts['imgs'] : null;
    $imgids   = explode(',', $imgIDStr);

    foreach ($imgids as $imgId) {
        $misc = get_post($imgId);

        $img = wp_get_attachment_image_src($imgId, 'full');

        $imgNew = [
            'src'     => $img[0],
            'caption' => $misc->post_excerpt,
            'url'     => $misc->post_content
        ];

        $imgs[] = $imgNew;
    }
?>

<div class="slider slider--std js-swiper--std">
    <div class="swiper-wrapper">
        <?php foreach ($imgs as $img): ?>
            <div class="swiper-slide slider__slide">
                <figure class="slider__slide-img img img--bg" style="background-image: url('<?php echo $img['src']; ?>')"></figure>
                <div class="slider__slide-caption">
                    <?php if ($img['url']): ?>
                        <a href="<?php echo $img['url']; ?>" class="slider__slide-link">
                    <?php endif ?>
                        <?php echo $img['caption']; ?>
                    <?php if ($img['url']): ?>
                        </a>
                    <?php endif ?>
                </div>
            </div>
        <?php endforeach ?>
    </div>

    <div class="swiper-pagination swiper-pagination--bg"></div>
</div>
