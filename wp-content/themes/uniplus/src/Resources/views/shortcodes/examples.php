<?php

    $examples = [
        'examples' => [],
        'sums' => [
            'normal'  => 0,
            'edu'     => 0,
            'savings' => 0
        ],
        'ps' => isset($atts['ps']) ? $atts['ps'] : null
    ];

    $children = explode(']', $content);
    array_pop($children);

    // create array
    foreach ($children as $child) {
        $child = json_decode(do_shortcode($child.']'), true);
        $child['savings'] = abs($child['normal'] - $child['edu']);

        $examples['examples'][]      = $child;

        $examples['sums']['normal']  += $child['normal'];
        $examples['sums']['edu']     += $child['edu'];
        $examples['sums']['savings'] += $child['savings'];
    }
?>

<div class="examples">
    <div class="examples__head">
        <div class="row">
            <div class="col-md-3 examples__head-item examples__head-item--cat">
                Kategória
            </div>

            <div class="col-md-3 examples__head-item examples__head-item--normal">
                Normál ügymenet
            </div>

            <div class="col-md-3 examples__head-item examples__head-item--edu">
                Edu+ program
            </div>

            <div class="col-md-3 examples__head-item examples__head-item--savings">
                Megtakarítás
            </div>
        </div>
    </div>

    <?php foreach ($examples['examples'] as $example): ?>
        <div class="examples__example">
            <div class="row">
                <div class="col-md-3 examples__example-cat">
                    <?php echo $example['category']; ?>
                </div>

                <div class="col-md-3 examples__example-item examples__example-item--normal">
                    <span class="examples__example-head">
                        Normál ügymenet:
                    </span>
                    <span class="examples__example-price">
                        <?php echo apply_filters('price', $example['normal']); ?> Ft
                    </span>
                </div>

                <div class="col-md-3 examples__example-item examples__example-item--edu">
                    <span class="examples__example-head">
                        Edu+ program:
                    </span>
                    <span class="examples__example-price">
                        <?php echo apply_filters('price', $example['edu']); ?> Ft
                    </span>
                </div>

                <div class="col-md-3 examples__example-item examples__example-item--saving">
                    <span class="examples__example-head">
                        Megtakarítás:
                    </span>
                    <span class="examples__example-price">
                        <?php echo apply_filters('price', $example['savings']); ?> Ft
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach ?>

    <div class="examples__sum">
        <div class="row">
            <div class="col-md-3 examples__sum-item examples__sum-item--title">
                Összesen
            </div>

            <div class="col-md-3 examples__sum-item examples__sum-item--normal">
                <span class="examples__sum-head">
                    Normál ügymenet:
                </span>
                <span class="examples__sum-price">
                    <?php echo apply_filters('price', $examples['sums']['normal']); ?> Ft
                </span>
            </div>

            <div class="col-md-3 examples__sum-item examples__sum-item--edu">
                <span class="examples__sum-head">
                    Edu+ program:
                </span>
                <span class="examples__sum-price">
                    <?php echo apply_filters('price', $examples['sums']['edu']); ?> Ft
                </span>
            </div>

            <div class="col-md-3 examples__sum-item examples__sum-item--savings">
                <span class="examples__sum-head">
                    Megtakarítás:
                </span>
                <span class="examples__sum-price">
                    <?php echo apply_filters('price', $examples['sums']['savings']); ?> Ft
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="examples__ps">
            <div class="col-xs-12">
                <?php echo $examples['ps']; ?>
            </div>
        </div>
    </div>
</div>
