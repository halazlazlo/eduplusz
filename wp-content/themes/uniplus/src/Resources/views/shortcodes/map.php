<?php
    $map = [
        'address' => esc_attr(get_option('address'))
    ];
?>

<div class="map" data-address="<?php echo $map['address']; ?>"></div>
