<?php

// src/Forms/ContactForm.php

namespace Uniplus\Forms;

use HL\WPAutoloader\Forms\AbstractForm;
use HL\WPAutoloader\Forms\iForm;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SurveyForm extends AbstractForm implements iForm
{
    public function __construct()
    {
        $emptyMsg = __("Can't be blank", 'uniplus');
        $now = new \DateTime();

        $this->options = [
            'menu_page' => [
                'label' => __('Survey messages', 'uniplus'),
                'icon' => 'dashicons-email-alt'
            ],
            'form' => [
                'id'     => 'survey_form',
                'method' => 'POST',
                'class'  => 'form--survey js-form',
                'fields' => [
                    'school' => [
                        'type' => 'text',
                        'placeholder' => __('School name', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'count_of_projectors' => [
                        'type' => 'number',
                        'placeholder' => __('Count of projectors', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'contact' => [
                        'type' => 'text',
                        'placeholder' => __('Contact person', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'phone' => [
                        'type' => 'tel',
                        'placeholder' => __('Phone', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'email' => [
                        'type' => 'email',
                        'placeholder' => __('Email', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg,
                            'email' => __('Invalid email address', 'uniplus')
                        ]
                    ],
                    'sent_at' => [
                        'type' => 'hidden',
                        'placeholder' => __('Sent at', 'uniplus'),
                        'value' => $now->format('Y-m-d H:i')
                    ]
                ]
            ]
        ];

        // export
        add_action('wp_ajax_'.$this->options['form']['id'].'_export', array($this, 'export'));
        add_action('wp_ajax_nopriv_'.$this->options['form']['id'].'_export', array($this, 'export'));
    }

    public function submit()
    {
        global $app;
        $formUtil = $app->getUtil('form');

        $ret = [
            'success' => true,
            'errors' => [],
        ];

        // fill form with values
        $formUtil->handleForm($this->options['form']);

        // validate
        $ret['errors'] = $formUtil->validate($this->options['form']);

        if (sizeof($ret['errors']) > 0) {
            $ret['success'] = false;
        } else {
            $surveyAnswer = $formUtil->getEntityToSave($this->options['form']);

            // send mail
            $email = [
                'survey_answer' => $surveyAnswer,
                'form' => $this->options['form'],
            ];

            $message = $app->render('emails/survey-answer.php', $email, true);

            $email = WP_DEBUG ? 'halaz.lazlo@gmail.com' : get_option('email');
            // $email = 'halaz.lazlo@gmail.com';
            wp_mail($email, __('New survey answer', get_template()), $message);

            // save message
            global $wpdb, $app;

            $wpdb->insert('survey_answers', $surveyAnswer);
        }

        echo json_encode($ret);
        exit;
    }

    public function list_entries()
    {
        global $app, $wpdb;

        // select
        $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
        $limit   = 20;
        $offset  = ($pagenum - 1) * $limit;

        $surveyAnswers = $wpdb->get_results(
            "SELECT * FROM survey_answers ORDER BY id DESC LIMIT $offset, $limit",
            ARRAY_A
        );

        // pagination
        $total = $wpdb->get_var("SELECT COUNT(`id`) FROM survey_answers");
        $num_of_pages = ceil( $total / $limit );

        $pagination = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __('&laquo;', 'uniplus'),
            'next_text' => __('&raquo;', 'uniplus'),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );

        // render
        $app->render('admin/forms/survey-answers.php', [
            'form'             => $this->options['form'],
            'contact_messages' => $surveyAnswers,
            'pagination'       => $pagination
        ]);
    }

    public function export()
    {
        if (!is_admin()) {
            echo "not";
            exit;
        }

        global $app, $wpdb;

        $surveyAnswers = $wpdb->get_results(
            "SELECT * FROM survey_answers ORDER BY id",
            ARRAY_A
        );

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // set header
        $col = 'A';
        $row = 1;
        foreach ($this->options['form']['fields'] as $key => $value) {
            $sheet->setCellValue($col.$row, __($key, 'uniplus'));

            $col++;
        }

        // set content
        foreach ($surveyAnswers as $answer) {
            $row++;
            $col = 'A';

            foreach ($this->options['form']['fields'] as $key => $value) {
                $sheet->setCellValue($col.$row, $answer[$key]);

                $col++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(BASE_PATH.'/../../uploads/spreadsheets/survey-answers.xlsx');

        header("Location: ".SITE_URI."/wp-content/uploads/spreadsheets/survey-answers.xlsx");
        exit;
    }
}
