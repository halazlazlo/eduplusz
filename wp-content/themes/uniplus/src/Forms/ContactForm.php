<?php

// src/Forms/ContactForm.php

namespace Uniplus\Forms;

use HL\WPAutoloader\Forms\AbstractForm;
use HL\WPAutoloader\Forms\iForm;

class ContactForm extends AbstractForm implements iForm
{
    public function __construct()
    {
        $emptyMsg = __("Can't be blank", 'uniplus');
        $now = new \DateTime();

        $this->options = [
            'menu_page' => [
                'label' => __('Contact messages', 'uniplus'),
                'icon' => 'dashicons-email-alt'
            ],
            'form' => [
                'id'     => 'contact_form',
                'method' => 'POST',
                'class'  => 'form--contact js-form',
                'fields' => [
                    'name' => [
                        'type' => 'text',
                        'placeholder' => __('Name', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'email' => [
                        'type' => 'email',
                        'placeholder' => __('Email', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg,
                            'email' => __('Invalid email address', 'uniplus')
                        ]
                    ],
                    'message' => [
                        'type' => 'textarea',
                        'placeholder' => __('Message', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'sent_at' => [
                        'type' => 'hidden',
                        'label' => __('Sent at', 'uniplus'),
                        'value' => $now->format('Y-m-d H:i')
                    ]
                ]
            ]
        ];
    }

    public function submit()
    {
        global $wpdb, $app;
        $formUtil = $app->getUtil('form');

        $ret = [
            'success' => true,
            'errors' => [],
        ];

        // fill form with values
        $formUtil->handleForm($this->options['form']);

        // validate
        $ret['errors'] = $formUtil->validate($this->options['form']);

        if (sizeof($ret['errors']) > 0) {
            $ret['success'] = false;
        } else {
            // save message
            $entytyToSave = $formUtil->getEntityToSave($this->options['form']);

            // send mail
            $email = [
                'entityToSave' => $entytyToSave,
                'form' => $this->options['form'],
            ];

            $message = $app->render('emails/contact-message.php', $email, true);

            $email = WP_DEBUG ? 'halaz.lazlo@gmail.com' : get_option('email');
            // $email = 'halaz.lazlo@gmail.com';

            $headers = ['From: EDU+ <nevalaszolj@eduplussz.hu>'];
            wp_mail($email, __('New contact email', get_template()), $message, $headers);

            $wpdb->insert('contact_messages', $entytyToSave);
        }

        echo json_encode($ret);
        exit;
    }

    public function list_entries()
    {
        global $app, $wpdb;

        // select
        $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
        $limit   = 20;
        $offset  = ($pagenum - 1) * $limit;

        $contactMessages = $wpdb->get_results(
            "SELECT * FROM contact_messages ORDER BY id DESC LIMIT $offset, $limit",
            ARRAY_A
        );

        // pagination
        $total = $wpdb->get_var("SELECT COUNT(`id`) FROM contact_messages");
        $num_of_pages = ceil( $total / $limit );

        $pagination = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __('&laquo;', 'uniplus'),
            'next_text' => __('&raquo;', 'uniplus'),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );

        // render
        $app->render('admin/forms/contact-messages.php', [
            'form'             => $this->options['form'],
            'contact_messages' => $contactMessages,
            'pagination'       => $pagination
        ]);
    }
}
