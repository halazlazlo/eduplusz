<?php

// src/Forms/ContactForm.php

namespace Uniplus\Forms;

use HL\WPAutoloader\Forms\AbstractForm;
use HL\WPAutoloader\Forms\iForm;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class MalfunctionForm extends AbstractForm implements iForm
{
    public function __construct()
    {
        $emptyMsg = __("Can't be blank", 'uniplus');
        $now = new \DateTime();

        $this->options = [
            'menu_page' => [
                'label' => __('Malfunction form entries', 'uniplus'),
                'icon' => 'dashicons-email-alt'
            ],
            'form' => [
                'id'     => 'malfunction_form',
                'method' => 'POST',
                'class'  => 'form--error js-form',
                'fields' => [
                    'school_name' => [
                        'type' => 'text',
                        'placeholder' => __('School name', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'school_address' => [
                        'type' => 'text',
                        'placeholder' => __('School address', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'contact' => [
                        'type' => 'text',
                        'placeholder' => __('Contact person', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'phone' => [
                        'type' => 'tel',
                        'placeholder' => __('Phone', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'email' => [
                        'type' => 'email',
                        'placeholder' => __('Email', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg,
                            'email' => __('Invalid email address', 'uniplus')
                        ]
                    ],
                    'projector_type' => [
                        'type' => 'text',
                        'placeholder' => __('Projector manufacturer and type (eg.: Epson EB-460)', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'malfunction' => [
                        'type' => 'textarea',
                        'placeholder' => __('Malfunction', 'uniplus').' *',
                        'validate' => [
                            'not_blank' => $emptyMsg
                        ]
                    ],
                    'sent_at' => [
                        'type' => 'hidden',
                        'placeholder' => __('Sent at', 'uniplus'),
                        'value' => $now->format('Y-m-d H:i')
                    ]
                ]
            ]
        ];

        // repo
        add_action('wp_ajax_find_one_by_school_name', array($this, 'find_one_by_school_name'));
        add_action('wp_ajax_nopriv_find_one_by_school_name', array($this, 'find_one_by_school_name'));
    }

    public function submit()
    {
        global $app;
        $formUtil = $app->getUtil('form');

        $ret = [
            'success' => true,
            'errors' => [],
        ];

        // fill form with values
        $formUtil->handleForm($this->options['form']);

        // validate
        $ret['errors'] = $formUtil->validate($this->options['form']);

        if (sizeof($ret['errors']) > 0) {
            $ret['success'] = false;
        } else {
            $entityToSave = $formUtil->getEntityToSave($this->options['form']);

            // send mail
            $email = [
                'entityToSave' => $entityToSave,
                'form' => $this->options['form'],
            ];

            $messageAdmin = $app->render('emails/malfunction-form-admin.php', $email, true);
            $messageUser = $app->render('emails/malfunction-form-user.php', $email, true);

            $email = WP_DEBUG ? 'halaz.lazlo@gmail.com' : 'hiba@eduplussz.hu';
            // $email = 'halaz.lazlo@gmail.com';

            $headers = array('From: EDU+ <hiba@eduplussz.hu>');
            wp_mail($email, __('New malfunction form entry', get_template()), $messageAdmin, $headers);
            wp_mail($entityToSave['email'], __('EDU+ malfunction entry', get_template()), $messageUser, $headers);

            // save message
            global $wpdb, $app;

            $wpdb->insert($this->options['form']['id'], $entityToSave);
        }

        echo json_encode($ret);
        exit;
    }

    public function list_entries()
    {
        global $app, $wpdb;

        // select
        $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
        $limit   = 20;
        $offset  = ($pagenum - 1) * $limit;

        $entries = $wpdb->get_results(
            "SELECT * FROM ".$this->options['form']['id']." ORDER BY id DESC LIMIT $offset, $limit",
            ARRAY_A
        );

        // pagination
        $total = $wpdb->get_var("SELECT COUNT(`id`) FROM ".$this->options['form']['id']);
        $num_of_pages = ceil( $total / $limit );

        $pagination = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __('&laquo;', 'uniplus'),
            'next_text' => __('&raquo;', 'uniplus'),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );

        // render
        $app->render('admin/forms/malfunction-form.php', [
            'form' => $this->options['form'],
            'entries' => $entries,
            'pagination' => $pagination
        ]);
    }

    public function find_one_by_school_name() {
        global $wpdb;

        $schoolName = esc_sql($_GET['schoolName']);

        $entry = $wpdb->get_row(
            "SELECT school_address, contact, phone, email
            FROM ".$this->options['form']['id']."
            WHERE school_name = '".$schoolName."'
            ORDER BY id DESC
            LIMIT 1",
            ARRAY_A
        );


        echo json_encode($entry);
        exit;
    }
}
