<?php

namespace Uniplus;

use HL\WPAutoloader\App as BaseApp;

use Uniplus\Forms\ContactForm;
use Uniplus\Forms\SurveyForm;
use Uniplus\Forms\MalfunctionForm;
use Uniplus\Shortcodes;

class App extends BaseApp
{
    public function start()
    {
        // init assets
        add_action('wp_enqueue_scripts', array($this, 'init_assets'), 100);

        // init shortcodes
        $this->services['shortcodes'] = new Shortcodes();
        $this->services['shortcodes']->init();

        // init forms
        $this->init_forms();
    }

    public function init_assets()
    {
        $min = '.min';
        if (WP_DEBUG) {
            $min = '';
        }

        wp_enqueue_style(
            'dcstyles',
            ASSET_URI.'/css/main'.$min.'.css',
            [],
            filemtime(ASSET_PATH.'/css/main'.$min.'.css'),
            'all'
        );

        if (WP_DEBUG) {
            wp_enqueue_script(
                'app_script_vendors',
                ASSET_URI.'/js/vendors.js',
                [],
                filemtime(ASSET_PATH.'/js/vendors.js'),
                'all'
            );

            wp_enqueue_script(
                'app_script_bundle',
                ASSET_URI.'/js/bundle.js',
                [],
                filemtime(ASSET_PATH.'/js/bundle.js'),
                'all'
            );
        } else {
            wp_enqueue_script(
                'app_script_eduplussz',
                ASSET_URI.'/js/main.min.js',
                [],
                filemtime(ASSET_PATH.'/js/main.min.js'),
                'all'
            );
        }
    }

    public function init_forms()
    {
        $this->forms['contact'] = new ContactForm();
        $this->forms['contact']->init();

        $this->forms['survey'] = new SurveyForm();
        $this->forms['survey']->init();

        $this->forms['malfunction'] = new MalfunctionForm();
        $this->forms['malfunction']->init();
    }
}
