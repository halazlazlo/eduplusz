<?php

$posts = get_posts([
    'post_type' => 'post'
]);

$next = get_next_post();
$prev = get_previous_post();

get_header(); ?>

<div class="container">
    <div class="post">
        <div class="post__sidebar">
            <ul class="menu sidebar__menu">
                <?php foreach ($posts as $sidebarPost): ?>
                    <li class="sidebar__menu-item">
                        <a href="/<?php echo $sidebarPost->post_name; ?>" class="sidebar__menu-link">
                            <?php echo $sidebarPost->post_title; ?>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="post__content">
            <h1 class="h h--1">
                <?php echo $post->post_title; ?>
            </h1>

            <div class="post__excerpt">
                <?php echo $post->post_excerpt; ?>
            </div>

            <?php echo apply_filters('the_content', wpautop($post->post_content)); ?>

            <ul class="menu post__links">
                <?php if ($prev): ?>
                    <li class="post__links-item">
                        <a href="<?php echo get_permalink($prev->ID) ?>" class="post__links-link">
                            Előző cikk
                        </a>
                    </li>
                <?php endif ?>

                <?php if ($next): ?>
                    <li class="post__links-item">
                        <a href="<?php echo get_permalink($next->ID) ?>" class="post__links-link">
                            Következő cikk
                        </a>
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>

<?php get_footer(); ?>
