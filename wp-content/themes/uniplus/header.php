<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <title><?php the_title(); ?> | <?php echo get_bloginfo() ?> | <?php echo get_bloginfo('description'); ?></title>

    <link href="<?php echo ASSET_URI.'/img/favico-32x32.png?ver='.filemtime(ASSET_PATH.'/img/favico-32x32.png'); ?>" rel="shortcut icon" type="image/png" sizes="16x16">
    <link href="<?php echo ASSET_URI.'/img/favico-32x32.png?ver='.filemtime(ASSET_PATH.'/img/favico-32x32.png'); ?>" rel="icon" type="image/png" sizes="32x32">

    <?php wp_head(); ?>

    <!-- analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-98637538-1', 'auto');
      ga('send', 'pageview');

    </script>
    <!-- /analytics -->
</head>
<?php
    global $app;

    $page = [
        'classes' => [
            'page',
            'page--'.$post->post_type
        ]
    ];

    if ($post->post_name) {
        $page['classes'][] = 'page--'.$post->post_name;
    }
    if ($post->ID) {
        $page['classes'][] = 'page--'.$post->ID;
    }

    $pageTemplateSlug = get_page_template_slug($post->ID);
    if ($pageTemplateSlug) {
        $page['classes'][] = 'page--'.str_replace('.php', '', $pageTemplateSlug);
    }
?>
<body class="<?php echo implode(' ', $page['classes']); ?>">
    <div class="hide">
        <?php include ASSET_PATH.'/svg/shapes/svg-defs.svg'; ?>
    </div>

    <div class="contact-link--env">
        <a href="/kapcsolat">
            <svg viewBox="0 0 100 100" class="icon">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#shape-envelope"></use>
            </svg>
        </a>
    </div>

    <div class="page__body">
        <div class="header">
            <div class="container">
                <div class="header__logo">
                    <a href="<?php echo get_home_url(); ?>">
                        <img src="<?php echo ASSET_URI.'/img/eduplus-logo.png'; ?>" alt="Eduplussz logo" class="img">
                    </a>
                </div>

                <input id="burger__input" class="burger__input" type="checkbox">

                <label for="burger__input" class="burger">
                    <span class="burger__bar"></span>
                </label>

                <div class="header__menu">
                    <div class="header__menu-body">
                        <div class="header__menu-bg"></div>
                        <?php
                            wp_nav_menu(
                                array(
                                    'menu'       => 'main',
                                    'container'  => false,
                                    'menu_class' => 'menu',
                                    'echo'       => true,
                                    'walker'     => new Uniplus\Utils\MenuUtil(),
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="page__content">
