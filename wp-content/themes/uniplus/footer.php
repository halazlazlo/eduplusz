        </div>

        <div class="footer">
            <?php if ($post->post_name === 'kapcsolat'): ?>
                <section class="footer__top footer__top--contact">
                    <div class="container">
                        <h2 class="h h--2 h--center">
                            Együttműködő partnereink
                        </h2>

                        <p class="footer__top-intro txt--std txt--center">
                            Az EDU+ program csak úgy működhet hatékonyan, ha támogatóink mellettünk állnak. Az ő anyagi segítségük teszi lehetővé,  hogy az oktatási intézmények azonnali, észrevehető méretű anyagi segítséget kapjanak.
                        </p>

                        <div class="footer__top-item">
                            <img src="<?php echo ASSET_URI.'/img/epson-logo.jpg'; ?>" alt="Epson logo" class="footer__top-img">
                            <p class="txt txt--std">
                                Kiemelt szakmai partnerünk az Epson Magyarország Kft., akik szakmai háttéranyagok biztosítása mellett kiemelt kedvezményeket adnak új projektorok vásárlásához, legyen szó normál vetítési távolságú vagy interaktív táblához tartozó short throw projektorokról.
                            </p>
                        </div>

                        <div class="footer__top-item">
                            <img src="<?php echo ASSET_URI.'/img/fuggetlen-szerviz-logo.jpg'; ?>" alt="Független szerviz logo" class="footer__top-img">
                            <p class="txt txt--std">
                                Stratégiai partnerünk a Függetlenszerviz Hungária Kft.
                            </p>
                        </div>
                    </div>
                </section>
            <?php else: ?>
                <div class="footer__top">
                    <div class="container">
                        <h2 class="h h--2">
                            Együttműködő partnereink
                        </h2>

                        <div class="footer__top-imgs">
                            <img src="<?php echo ASSET_URI.'/img/epson-logo.jpg'; ?>" alt="Epson logo" class="footer__top-img">
                            <img src="<?php echo ASSET_URI.'/img/fuggetlen-szerviz-logo.jpg'; ?>" alt="Független szerviz logo" class="footer__top-img">
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <div class="footer__bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="footer__bottom-left">
                                <div class="row align-items-center">
                                    <div class="col-lg-3">
                                        <img src="<?php echo ASSET_URI.'/img/uniplus-logo.png'; ?>" alt="Uniplus logo" class="footer__logo img">
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="footer__copyright">
                                            <?php echo get_option('copyright'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5">
                            <div class="footer__bottom-right">
                                <?php if (get_option('facebook')): ?>
                                    <div class="footer__bottom-block">
                                        <a href="<?php echo get_option('facebook'); ?>" class="footer__bottom-link footer__bottom-link--lg">
                                            Facebook
                                        </a>
                                    </div>
                                <?php endif ?>

                                <div class="footer__bottom-block">
                                    <?php echo get_option('address'); ?>
                                </div>
                                <div class="footer__bottom-block">
                                    <span class="footer__bottom-item">
                                        <a href="tel:<?php echo get_option('phone'); ?>" class="footer__bottom-link">
                                            <?php echo get_option('phone'); ?>
                                        </a>
                                    </span>

                                    <span class="footer__bottom-item">
                                        <a href="mailto:<?php echo get_option('email'); ?>" class="footer__bottom-link">
                                            <?php echo get_option('email'); ?>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- scripts -->
    <script>
        var baseUrl = '<?php echo get_template_directory_uri(); ?>';
        var apiUrl = '<?php echo admin_url("admin-ajax.php"); ?>'
    </script>

    <?php wp_footer(); ?>
    <!-- /scripts -->
</body>
</html>
